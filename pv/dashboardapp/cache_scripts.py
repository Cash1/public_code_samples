"""
Script to get data for dashboard caching
"""

import calendar
import locale
from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.core.cache import cache
from django.db.models import Sum, Count, Avg, Max
from django.utils import timezone

from salesapp.models import PGFuels, PGStations, PGSales, Last30DaysRevenue, \
    AveragesByStation, Averagesbymonthbystation


def init_cache():
    """
    Base data for caches
    :return:
    """

    allstations = PGStations.objects.filter(id__gt=0,
                                            lastexchange__isnull=False)
    fuels = PGFuels.objects.all()
    cache.set('stations', allstations, timeout=None)
    cache.set('fuels', fuels, timeout=None)


def daily_update_sales_cache():
    """
    Sales data for daily update
    :return:
    """

    allstations = PGStations.objects.filter(id__gt=0,
                                            lastexchange__isnull=False)
    fuels = PGFuels.objects.all()
    cache.set('stations', allstations)
    cache.set('fuels', fuels)
    today = timezone.now()
    markupdate = datetime(year=today.year, month=today.month, day=1,
                          tzinfo=timezone.utc)
    last_month_start = markupdate - relativedelta(months=1)
    last_month_markup = today - relativedelta(months=1)
    start_this_week = (today - relativedelta(days=6)).replace(hour=0, minute=0,
                                                              second=0)
    last_week_start = start_this_week - relativedelta(days=7)
    markup_last_week = today - relativedelta(days=7)

    if timezone.now().hour > 8:
        current_shift = PGSales.objects.values('idshift').aggregate(
            Max('idshift'))
        cache.set('current_shift', current_shift, timeout=(60 * 60 * 24))
        current_month_stats = get_stats_for_period(markupdate, today)
        current_week_stats = get_stats_for_period(start_this_week, today)
        cache.set('current_month_revenue',
                  current_month_stats['revenue'],
                  timeout=(60 * 60 * 24))
        cache.set('current_month_clients',
                  current_month_stats['clients'],
                  timeout=(60 * 60 * 24))
        cache.set('current_month_dose',
                  current_month_stats['dose'],
                  timeout=(60 * 60 * 24))
        cache.set('current_month_average_purchase',
                  current_month_stats['avg_rev'],
                  timeout=(60 * 60 * 24))
        cache.set('current_month_average_dose',
                  current_month_stats['avg_dose'],
                  timeout=(60 * 60 * 24))
        cache.set('current_week_revenue',
                  current_week_stats['revenue'],
                  timeout=(60 * 60 * 24))
        cache.set('current_week_clients',
                  current_week_stats['clients'],
                  timeout=(60 * 60 * 24))
        cache.set('current_week_dose',
                  current_week_stats['dose'],
                  timeout=(60 * 60 * 24))
        cache.set('current_week_average_purchase',
                  current_week_stats['avg_rev'],
                  timeout=(60 * 60 * 24))
        cache.set('current_week_average_dose',
                  current_week_stats['avg_dose'],
                  timeout=(60 * 60 * 24))

        last_month_stats = get_stats_for_period(last_month_start,
                                                last_month_markup)
        last_week_stats = get_stats_for_period(last_week_start,
                                               markup_last_week)
        cache.set('last_month_revenue',
                  last_month_stats['revenue'], timeout=(60 * 60 * 24))
        cache.set('last_month_clients',
                  last_month_stats['clients'], timeout=(60 * 60 * 24))
        cache.set('last_month_dose',
                  last_month_stats['dose'], timeout=(60 * 60 * 24))
        cache.set('last_month_average_purchase',
                  last_month_stats['avg_rev'], timeout=(60 * 60 * 24))
        cache.set('last_month_average_dose',
                  last_month_stats['avg_dose'], timeout=(60 * 60 * 24))
        cache.set('last_week_revenue',
                  last_week_stats['revenue'], timeout=(60 * 60 * 24))
        cache.set('last_week_clients',
                  last_week_stats['clients'], timeout=(60 * 60 * 24))
        cache.set('last_week_dose',
                  last_week_stats['dose'], timeout=(60 * 60 * 24))
        cache.set('last_week_average_purchase',
                  last_week_stats['avg_rev'], timeout=(60 * 60 * 24))
        cache.set('last_week_average_dose',
                  last_week_stats['avg_dose'], timeout=(60 * 60 * 24))
        averages_by_station = AveragesByStation.objects.values('avg',
                                                               'revenue',
                                                               'station').all()
        average_daily_sales = {station['station']: station['avg']
                               for station in averages_by_station}
        cache.set('average_daily_sales',
                  average_daily_sales, timeout=(60 * 60 * 24))

        averages_by_month_by_station = Averagesbymonthbystation.objects.values(
            'year', 'month', 'station', 'average_dose',
            'average_revenue').all().order_by('year', 'month')
        sales_by_month = {}
        for station in cache.get('stations'):
            sales_by_month[station.name] = ['None'] * 12
        for station_average in averages_by_month_by_station:
            for station in cache.get('stations'):
                if station_average['station'] == station.id:
                    if station_average['month'] != datetime.today().month:
                        sales_by_month[station.name][station_average['month'] - 1] = str(
                            round(station_average['average_dose'] / calendar.monthrange(
                                station_average['year'], station_average['month'])[1], 0))
                    else:
                        sales_by_month[station.name][station_average['month'] - 1] = str(
                            round(station_average['average_dose'] / datetime.today().day, 0))
        cache.set('sales_by_month', sales_by_month, timeout=(60 * 60 * 24))

        locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
        months_names = [month for month in calendar.month_name]
        shifted_months = list(range(timezone.now().month + 1, 13)) + \
                         list(range(1, timezone.now().month + 1))
        months = [months_names[month - 1] for month in shifted_months]
        cache.set('months', months, timeout=(60 * 60 * 24))
        cache.set('daily_cache_updated_at',
                  timezone.now(), timeout=(60 * 60 * 24))


def hourly_update_sales_cache():
    """
    Sales data for hourly update.
    :return:
    """

    last_30_days = Last30DaysRevenue.objects.values('revenue', 'date',
                                                    'station',
                                                    'dose').order_by('date',
                                                                     'station')
    cache.set('last_30_days', last_30_days, timeout=(60 * 60))

    last_30_dates = []
    for row in last_30_days:
        if str(row['date']) not in last_30_dates:
            last_30_dates.append(str(row['date']))
    cache.set('last_30_dates', last_30_dates, timeout=(60 * 60))

    lsdict = {station.name: [round(float(row['dose']), 2)
                             for row in last_30_days
                             if row['station'] == station.name]
              for station in cache.get('stations')}
    cache.set('lsdict', lsdict, timeout=(60 * 60))
    cache.set('hourly_cache_updated_at', timezone.now(), timeout=(60 * 60))


def get_stats_for_period(markupdate, day):
    """
    Get sales data for set period of time
    :param markupdate:
    :param day:
    :return:
    """

    return PGSales.objects.filter(
        datetimestop__range=[markupdate, day],
        idstation__gt=0).aggregate(revenue=Sum("amount2"),
                                   clients=Count("amount2"),
                                   dose=Sum("dose2"),
                                   avg_dose=Avg("dose2"),
                                   avg_rev=Avg("amount2"))
