from django.db.models import Sum, Count
from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.safestring import mark_safe
from django.conf import settings
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.utils import timezone
from django.urls import reverse
from django.core.paginator import Paginator
from django.core import serializers

from dashboard.decorators import *
from dashboard.scripts import cache_scripts
from salesapp.models import PGFuels, PGSales, PGStations, PGShifts
from stationsapp.models import FuelBalances, RemoteTanks

import loyalty

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@login_required
def dashboard(request):
    if request.user.groups.all().filter(name='Card manager').exists():
        return loyalty.views.list_of_bonus_clients(request)
    elif request.user.groups.all().filter(name='Chief_of_station').exists():
        return chief_of_station_dashboard(request)
    else:
        return dashboard_cached(request)


@login_required
def dashboard_cached(request):
    allstations = PGStations.objects.filter(id__gt=0,
                                            lastexchange__isnull=False)
    revenue_performance = round((cache.get('current_month_revenue') / cache.get('last_month_revenue') - 1) * 100, 2)
    total_clients_performance = round((cache.get('current_month_clients') / cache.get('last_month_clients') - 1) * 100, 2)
    dose_performance = round((cache.get('current_month_dose') / cache.get('last_month_dose') - 1) * 100, 2)
    average_purchase_performance = round((cache.get('current_month_average_purchase') /
                                          cache.get('last_month_average_purchase') - 1) * 100, 2)
    average_dose_performance = round((cache.get('current_month_average_dose') /
                                      cache.get('last_month_average_dose') - 1) * 100, 2)
    if cache.get('last_week_clients') != 0:
        total_clients_performance_week = round((cache.get('current_week_clients') /
                                                cache.get('last_week_clients') - 1) * 100, 2)
    else:
        total_clients_performance_week = 0
    if cache.get('last_week_revenue') != 0:
        revenue_performance_week = round((cache.get('current_week_revenue') /
                                          cache.get('last_week_revenue') - 1) * 100, 2)
    else:
        revenue_performance_week = 0
    if cache.get('last_week_dose') != 0:
        dose_performance_week = round((cache.get('current_week_dose') /
                                       cache.get('last_week_dose') - 1) * 100,
                                      2)
    else:
        dose_performance_week = 0
    if cache.get('last_week_average_purchase') != 0:
        average_purchase_performance_week = round((cache.get('current_week_average_purchase') /
                                                   cache.get('last_week_average_purchase') - 1) * 100, 2)
    if cache.get('last_week_average_dose') != 0:
        average_dose_performance_week = round((cache.get('current_week_average_dose') /
                                               cache.get('last_week_average_dose') - 1) * 100, 2)
    else:
        average_dose_performance_week = 0
    stationsstatus = {station.name: station.lastexchange for station in allstations}
    station_offline_status = [station.name for station in allstations if
                              ((timezone.now() - station.lastexchange).total_seconds() / 3600 > 1)]
    context = {
        'stations': allstations,
        'stationsstatus': stationsstatus,
        'current_month_revenue': cache.get('current_month_revenue'),
        'revenue_performance': revenue_performance,
        'total_clients': cache.get('current_month_clients'),
        'total_clients_performance': total_clients_performance,
        'current_month_dose': cache.get('current_month_dose'),
        'dose_performance': dose_performance,
        'average_purchase': cache.get('current_month_average_purchase'),
        'average_purchase_performance': average_purchase_performance,
        'average_dose': cache.get('current_month_average_dose'),
        'average_dose_performance': average_dose_performance,
        'current_week_revenue': cache.get('current_week_revenue'),
        'revenue_performance_week': revenue_performance_week,
        'total_clients_this_week': cache.get('current_week_clients'),
        'total_clients_performance_week': total_clients_performance_week,
        'average_purchase_this_week': cache.get('current_week_average_purchase'),
        'average_purchase_performance_week': average_purchase_performance_week,
        'current_week_dose': cache.get('current_week_dose'),
        'dose_performance_week': dose_performance_week,
        'average_dose_week': cache.get('current_week_average_dose'),
        'average_dose_performance_week': average_dose_performance_week,
        'station_offline_status': station_offline_status,
        'last_month_dates': mark_safe(cache.get('last_30_dates')),
        'last_30_days_rev': mark_safe(cache.get('lsdict')),
        'average_daily_sales': cache.get('average_daily_sales'),
        'sales_by_month': mark_safe(cache.get('sales_by_month')),
        'months': mark_safe(cache.get('months')),
        'daily_cache_updated_at': cache.get('daily_cache_updated_at'),
        'hourly_cache_updated_at': cache.get('hourly_cache_updated_at'),
    }

    return render(request, 'dashboard/dashboard2.html', context)


@login_required
def chief_of_station_dashboard(request):
    current_station = request.user.stations
    station_object = PGStations.objects.filter(id=current_station,
                                               lastexchange__isnull=False)

    fuels = PGFuels.objects.all().order_by('id')
    fuel_tanks = {}

    last_shift = PGShifts.objects.filter(
        station=current_station).order_by('-id')[0]
    last_shift = PGShifts.objects.filter(
        station=current_station, id=round(last_shift.id, -1))[0]

    # Данные по продажам
    sales_for_current_shift = PGSales.objects.filter(
        idstation=current_station,
        idshift__gte=(last_shift.id+10),
        idtypepay__in=[1, 5, 9, 10, 11, 12]).values('idfuel').order_by('idfuel').annotate(dose=Sum('dose2')*(-1))
    total_sales_for_current_shift = PGSales.objects.filter(
        idstation=current_station,
        idshift__gte=(last_shift.id+10),
        idtypepay__in=[1, 4, 5, 9, 10, 11, 12]).aggregate(sum=Sum('dose2')*(-1))['sum']
    revenue_for_current_shift = PGSales.objects.filter(
        idstation=current_station,
        idshift__gte=(last_shift.id+10),
        idtypepay__in=[1, 5, 9, 10, 11, 12]).aggregate(revenue=Sum('amount2'))
    current_shift_count = PGSales.objects.filter(
        idstation=current_station,
        idshift__gte=(last_shift.id+10),
        idtypepay__in=[1, 5, 9, 10, 11, 12],
        dose2__lt=0).aggregate(count=Count('id'))
    cash_revenue = PGSales.objects.filter(
        idstation=current_station,
        idshift__gte=(last_shift.id+10),
        idtypepay__in=[1, 9], dose2__lt=0).aggregate(sum=Sum('amount2'))['sum']
    bank_card_revenue = PGSales.objects.filter(
        idstation=current_station,
        idshift__gte=(last_shift.id+10),
        idtypepay__in=[5, 10],
        dose2__lt=0).aggregate(sum=Sum('amount2'))['sum']
    fuel_card_revenue = PGSales.objects.filter(
        idstation=current_station,
        idshift__gte=(last_shift.id+10),
        idtypepay__in=[11],
        dose2__lt=0).aggregate(sum=Sum('amount2'))['sum']
    if ((timezone.now() - station_object[0].lastexchange).total_seconds() / 3600) < 1:
        station_status = {'status': True,
                          'update_time': station_object[0].lastexchange}
    else:
        station_status = {'status': False,
                          'update_time': station_object[0].lastexchange}
    tanks = RemoteTanks.objects.using('firebird').values('id_fuel',
                                                         'maxvolume').filter(id_station=current_station)

    tanks_dict = {tank['id_fuel']: tank['maxvolume'] for tank in tanks}

    def serialize_station_tanks_to_list(tanks_dict,
                                        fuels,
                                        sales,
                                        fuel_measurements):
        balances = []
        for tdict in tanks_dict:
            was_sold = False
            for measurement in fuel_measurements:
                for sale in sales:
                    if sale['idfuel'] == tdict and measurement.fuel == tdict:
                        tank = {
                            'name': fuels.filter(id=tdict)[0].name,
                            'max': tanks_dict[tdict],
                            'now': round(measurement.qty - sale['dose'], 0),
                            'share': (measurement.qty - sale['dose']) / tanks_dict[tdict] * 100,
                            'id_fuel': tdict,
                        }
                        was_sold = True
                    if not was_sold:
                        tank = {'name': fuels.filter(id=tdict)[0].name,
                                'max': tanks_dict[tdict],
                                'now': round(measurement.qty, 0),
                                'share': measurement.qty / tanks_dict[tdict] * 100,
                                'id_fuel': tdict, }
                    balances.append(tank)

        return balances
    # Замеры остатков топлива
    if FuelBalances.objects.filter(station=current_station):
        fuel_balances = FuelBalances.objects.filter(
            station=current_station).order_by('-date')[:4]

        # Проверка: если баланс внесен до перехода на время замера, продажи берем от времени открытия смены в этот день

        if (fuel_balances[0].date + timezone.timedelta(hours=3)).hour == 0:
            last_measurement_date = fuel_balances[0].date + timezone.timedelta(days=1, hours=3)

            balancedate = fuel_balances[0].date + timezone.timedelta(days=1, hours=3)
            shift = PGShifts.objects.filter(station=current_station,
                                            closeddate__year=balancedate.year,
                                            closeddate__month=balancedate.month,
                                            closeddate__day=balancedate.day).order_by('-id')[0]
            sale_since_measurement = PGSales.objects.filter(
                idstation=current_station,
                idshift__gte=round(shift.id, -1) + 10).values('idfuel').annotate(dose=Sum('dose2') * (-1))

        else:  # если баланс внесен после перехода на время замера. Продажи берем от времени замера
            last_measurement_date = fuel_balances[0].date + timezone.timedelta(hours=3)
            sale_since_measurement = PGSales.objects.filter(
                idstation=current_station,
                datetimestop__gte=fuel_balances[0].date).values('idfuel').annotate(dose=Sum('dose2') * (-1))
            fuel_balances = serialize_station_tanks_to_list(tanks_dict,
                                                            fuels,
                                                            sale_since_measurement,
                                                            fuel_balances)

    else:
        fuel_balances = []
        last_measurement_date = None

    fuels_set = []
    for fuel in fuels:
        for sale in sales_for_current_shift:
            if sale['idfuel'] == fuel.id:
                fuels_set.append(fuel.name)

    current_sales = {
        'fuels': tuple(fuels_set),
        'sales': tuple(sale['dose'] for sale in sales_for_current_shift),
        'revenue': revenue_for_current_shift['revenue'],
    }

    context = {
        'current_station': station_object[0],
        'status': station_status,
        'fuel_balances': fuel_balances,
        'current_sales': current_sales if current_sales else 0,
        'fuel_tanks': fuel_tanks,
        'last_measurement_date': last_measurement_date - timezone.timedelta(hours=3) if last_measurement_date else '',
        'total_sales_for_current_shift': total_sales_for_current_shift if total_sales_for_current_shift else 0,
        'current_shift_count': current_shift_count if current_shift_count else 0,
        'cash_revenue': cash_revenue if cash_revenue else 0,
        'bank_card_revenue': bank_card_revenue if bank_card_revenue else 0,
        'fuel_card_revenue': fuel_card_revenue if fuel_card_revenue else 0,
    }
    return render(request, 'stationsapp/chief_of_station_dashboard.html',
                  context)


def save_station_measurement(request):
    if request.method == 'POST':
        fuel_tanks = RemoteTanks.objects.using('firebird').filter(
            id_station=request.user.stations)
        for tank in fuel_tanks:
            fuel_balance = FuelBalances(date=request.POST['date'],
                                        fuel=tank.id_fuel,
                                        qty=request.POST[str(tank.id_fuel)],
                                        station=request.user.stations)
            fuel_balance.save()
    return HttpResponseRedirect(reverse('dashboard:view'))


@login_required
def shift_reports(request):
    shifts = PGShifts.objects.filter(station=request.user.stations).order_by('-id')
    shifts_json = serializers.serialize('json', shifts, fields=('id',
                                                                'openeddate',
                                                                'closeddate'))
    paginator = Paginator(shifts, 5)
    page = request.GET.get('page')
    context = {
        'shifts': paginator.get_page(page),
        'shifts_json': mark_safe(shifts_json),
    }
    return render(request,
                  'stationsapp/includes/shift_report_list_widget.html',
                  context)


@login_required
def get_fuel_balances_form(request):
    """
    Generate fuel balances input page
    :param request:
    :return:
    """
    tanks = RemoteTanks.objects.using('firebird').filter(
        id_station=request.user.stations).order_by('id_fuel')
    fuels = PGFuels.objects.all().order_by('id')
    station_object = PGStations.objects.filter(id=request.user.stations,
                                               lastexchange__isnull=False)[0]
    fuel_tanks = []
    for tank in tanks:
        tdict = {'id': tank.id_fuel,
                 'name': fuels.filter(id=tank.id_fuel)[0].name}
        fuel_tanks.append(tdict)
    context = {
        'fuel_tanks': fuel_tanks,
        'current_station': station_object,
    }
    return render(request, 'stationsapp/includes/fuel_balances_form.html',
                  context)


@login_required()
def refresh_cache(request):
    """
    Manual cache update function
    :param request:
    :return:
    """
    cache_scripts.init_cache()
    cache_scripts.daily_update_sales_cache()
    cache_scripts.hourly_update_sales_cache()
    return HttpResponseRedirect('/')
