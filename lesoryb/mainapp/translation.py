from modeltranslation.translator import register, TranslationOptions
from mainapp.models import Arena


@register(Arena)
class ArenaTranslationOptions(TranslationOptions):
    fields = ('rusname',)

