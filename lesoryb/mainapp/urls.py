from django.urls import re_path
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

import mainapp.views as views

app_name = 'mainapp'

urlpatterns = [
    re_path('^$', views.index, name='index'),
    re_path('^rules', views.rules, name='rules'),
    re_path('^contact', views.contact, name='contact'),
    re_path('^privacy', views.privacy, name='privacy'),
    re_path('^games', views.games, name='games'),
    re_path('^cookies', views.cookies, name='cookies'),
    re_path('^news/$', views.newslist, name='newslist'),
    re_path('^callme', views.callme, name='callme'),
    re_path('^book', views.book, name='book'),
    re_path('^birthday', views.birthday, name='birthday'),
    re_path('^giftcards', views.giftcards, name='giftcards'),
    re_path('^news/(?P<pk>\d+)/', views.news, name='newslist'),
    re_path('^sitemap.xml', views.sitemap, name='sitemap'),
    re_path('^robots.txt$', TemplateView.as_view(
        template_name='mainapp/robots.txt')),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
