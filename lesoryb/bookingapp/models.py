# import datetime

from django.utils import timezone
from django.db import models
from django.contrib.postgres.fields import DateTimeRangeField, \
    DateTimeRangeNoZoneField
from mainapp.models import Arena
from authapp.models import LesorubUser


class Order(models.Model):
    booker = models.ForeignKey(LesorubUser, null=False, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
    payment_status = models.CharField(max_length=10, default='none')

    def __str__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Order'
        managed = True
        db_table = 'order'


class Booking(models.Model):
    arenaname = models.ForeignKey(Arena, blank=False,
                                  null=False, on_delete=models.DO_NOTHING)
    bookdatetime = models.DateTimeField(null=False)
    duration = DateTimeRangeNoZoneField(null=True)
    used_status = models.CharField(max_length=10, default='no')
    qtyofguests = models.SmallIntegerField(default=1)
    order_id = models.ForeignKey(Order, null=False,
                                 on_delete=models.CASCADE, default=0)

    def __str__(self):
        return self.arenaname.name

    def __repr__(self):
        return self.arenaname.name

    class Meta:
        verbose_name = 'Booking'
        managed = True
        db_table = 'booking'
