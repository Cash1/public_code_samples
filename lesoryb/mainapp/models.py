from django.db import models

# Create your models here.


class Arena(models.Model):
    name = models.CharField(max_length=10, blank=False)
    rusname = models.CharField(max_length=10, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        managed = True
        db_table = 'arena'


class InstagramPost(models.Model):
    code = models.CharField(max_length=50, blank=False)
    created = models.DateField(blank=True, null=True)


    def __str__(self):
        return self.code

    class Meta:
        managed = True
        db_table = 'instagrampost'


class NewsArticle(models.Model):
    title = models.TextField(max_length=5000, blank=False)
    img = models.ImageField(blank=True)
    tags = models.ManyToManyField(to='NewsTag', blank=True)
    text = models.TextField(max_length=5000, blank=False)
    postdate = models.DateField()
    htmlcode = models.TextField(max_length=50000, blank=True)


    def __str__(self):
        return self.title

    class Meta:
        managed = True
        db_table = 'newsarticle'


class NewsTag(models.Model):
    tag = models.CharField(max_length=20, blank=False)
    posts = models.ManyToManyField(to='NewsArticle', blank=True)

    def __str__(self):
        return self.tag

    class Meta:
        managed = True
        db_table = 'newstag'
