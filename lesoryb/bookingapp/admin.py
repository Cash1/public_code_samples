from django.contrib import admin
from bookingapp.models import Booking, Order


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = ('arenaname', 'bookdatetime', 'used_status')
    list_filter = ('used_status', 'arenaname')


class BookingInline(admin.TabularInline):
    model = Booking


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'date', 'booker', 'payment_status')
    list_filter = ('payment_status', 'date', 'booker')
    inlines = [BookingInline, ]
