from pytz import timezone
import datetime

from django.core.management.base import BaseCommand
from django.db import connections
from django.db.models import Exists, Max
from django.db.models.fields.related import ForeignKey

from salesapp import scripts as salesscripts
import salesapp.models as salesmodels


class Command(BaseCommand):
    def _add_timezone(self, dicts):
        l = len(dicts)
        for index, ds in enumerate(dicts):
            for key, element in ds.items():
                if isinstance(element, datetime.datetime):
                    ds[key] = ds[key].replace(tzinfo=timezone('Etc/GMT-3'))
        return dicts

    def handle(self, *args, **options):
        sync_templates = salesmodels.PGModelSyncStatus.objects.all()
        f_cursor = connections['firebird'].cursor()

        for template in sync_templates:
            if template.sync_mode == 'insertnew':

                # Model from Postgres Database
                pgmodel = getattr(salesmodels, template.modelname[1])
                # Model from Firebird Database
                fbmodel = getattr(salesmodels, template.modelname[0])

                latest_date_in_database = pgmodel.objects.aggregate(Max('{}'.format(template.sync_field_check)))[template.sync_field_check+'__max']

                latest_date_in_database = latest_date_in_database.replace(tzinfo=timezone('Europe/Moscow'))

                latest_date_in_database = datetime.datetime.strftime(latest_date_in_database.replace(hour=latest_date_in_database.hour), '%Y-%m-%d %H:%M:%S')

                f_cursor.execute("SELECT * from {} where {} > \'{}\'".format(template.modelname[0], template.sync_field_check, latest_date_in_database))

                # Convert queryset to dictionary

                fbdata = salesscripts.dictfetchall(f_cursor)
                fbdata = self._add_timezone(fbdata)
                relational_fields = {}

                # Get list of related models via Foreign Key Field
                for field in pgmodel._meta._get_fields():
                    if isinstance(field, ForeignKey):
                        relational_fields[field] = field.related_model

                # Check if related models are required, if not - create new database transactions

                if len(relational_fields) == 0:
                    transactions = [pgmodel(**transaction) for transaction in fbdata]
                else:
                    transactions = []
                    for transaction in fbdata:
                        for field in relational_fields:
                            related_model = relational_fields[field]
                            transaction[field.name] = related_model.objects.get(pk=transaction[field.name])

                        if pgmodel._meta.unique_together:
                            dict_of_pk_data = {_ :transaction[_] for _ in pgmodel._meta.unique_together[0]}
                        else:
                            dict_of_pk_data = {'id': transaction['id']}

                        if not pgmodel.objects.filter(**dict_of_pk_data).exists():
                            transactions.append(pgmodel(**transaction))
                pgmodel.objects.bulk_create(transactions)
            elif template.sync_mode == 'upsert':
                pgmodel = getattr(salesmodels, template.modelname[1])
                f_cursor.execute(
                    "SELECT * from {}".format(template.modelname[0]))

                # Convert queryset to dictionary

                fbdata = salesscripts.dictfetchall(f_cursor)
                fbdata = self._add_timezone(fbdata)
                relational_fields = {}

                # Get list of related models via Foreign Key Field
                for field in pgmodel._meta._get_fields():
                    if isinstance(field, ForeignKey):
                        relational_fields[field] = field.related_model

                transactions = []
                for transaction in fbdata:
                    # Getting related objects
                    if len(relational_fields) > 0:
                        for relational_field in relational_fields:
                            related_model = relational_fields[relational_field]
                            transaction[relational_field.name] = related_model.objects.get(pk=transaction[relational_field.name])

                    if pgmodel._meta.unique_together:
                        dict_of_pk_data = {_ :transaction[_] for _ in pgmodel._meta.unique_together[0]}
                    else:
                        dict_of_pk_data = {'id': transaction['id']}

                    # Checking if transaction exists. If not, updating the necessary fields

                    if not pgmodel.objects.filter(**dict_of_pk_data).exists():
                        transactions.append(pgmodel(**transaction))
                    else:
                        dict_to_sync = {field.name: transaction[field.name] for field.name in template.fields_to_sync}
                        pgmodel.objects.filter(**dict_of_pk_data).update(**dict_to_sync)
                # Inserting new transactions
                pgmodel.objects.bulk_create(transactions)
