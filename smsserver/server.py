"""
SMS server for Huawei usb dongle
"""

import json
from collections import OrderedDict
import logging
import subprocess
import time
import datetime
from functools import wraps

import xmltodict
import requests
import psutil
import psycopg2
import psycopg2.extras as extras
from bs4 import BeautifulSoup
from alchemymodels import SMSAlchemy

LOG = logging.getLogger('serverlogger')


class SMS(SMSAlchemy):
    """
    SMS class
    """
    def __init__(self, message='', phone='', **kwargs):
        self.content = message
        self.phone = phone
        self.date = datetime.datetime.now()
        self.len = len(message)
        self.index = 0
        self.savetype = 4
        self.priority = 0
        self.smstype = 1
        for kwarg in kwargs:
            setattr(self, kwarg.lower(), kwargs[kwarg])


class API:
    """
    USB dongle API class
    """
    def __init__(self):
        self.home_url = 'http://{}/'
        self.sms_url = 'html/smsinbox.html'
        self.ussd_url = 'html/ussd.html'
        self.monitoring_status = 'api/monitoring/status/'
        self.session_api = 'webserver/SesTokInfo/'
        self.sms_send_url = 'api/sms/send-sms'
        self.sms_receive_url = 'api/sms/sms-list'
        self.sms_count_url = 'api/sms/sms-count'
        self.sms_delete_url = 'api/sms/delete-sms'
        self.sms_set_read_url = 'api/sms/set-read'
        self.get_sms_xml = '<?xml version="1.0" encoding="UTF-8"?><request>' \
                           '<PageIndex>1</PageIndex><ReadCount>{}' \
                           '</ReadCount><BoxType>1</BoxType><SortType>0' \
                           '</SortType><Ascending>0</Ascending>' \
                           '<UnreadPreferred>1</UnreadPreferred></request>'
        self.get_sent_sms_xml = '<?xml version="1.0" encoding="UTF-8"?>' \
                                '<request><PageIndex>1</PageIndex>' \
                                '<ReadCount>{}</ReadCount><BoxType>2' \
                                '</BoxType><SortType>0</SortType><Ascending>0'\
                                '</Ascending><UnreadPreferred>0' \
                                '</UnreadPreferred></request>'
        self.sms_id_xml = '<?xml version="1.0" encoding="UTF-8"?><request>' \
                          '<Index>{}</Index></request>'
        self.sms_send_xml = '<?xml version="1.0" encoding="UTF-8"?><request>' \
                            '<Index>-1</Index><Phones><Phone>{}' \
                            '</Phone></Phones><Sca></Sca><Content>{}' \
                            '</Content><Length>{}</Length><Reserved>1' \
                            '</Reserved><Date>{}</Date></request>'
        self.ussd_send_api = 'http://192.168.8.1/api/ussd/send'
        self.ussd_send_xml = '<?xml version="1.0" encoding="UTF-8"?>' \
                             '<request><content>{}</content><codeType>' \
                             'CodeType</codeType><timeout></timeout></request>'
        self.ussd_get_api = 'http://192.168.8.1/api/ussd/get'


class PGDB:
    """
    PostgresDatabase connection class
    """
    def __init__(self):
        with open('server_settings.json', 'r') as file:
            settings = json.loads(file.read())
        dbformat = 'postgres'
        dbsetup = settings['database_remote']
        if dbformat is not None and dbformat == 'postgres':
            self.dbsetup = dbsetup
            self.db_connection = psycopg2.connect(**dbsetup)
            self.cursor = self.db_connection.cursor()
            self.cursor2 = self.db_connection.cursor(
                cursor_factory=extras.DictCursor)
            self.db_connected = True
            self.db_connection.close()


class Server(API, PGDB):
    """
    Server class
    """
    def __init__(self, donglehost='localhost'):
        API.__init__(self)
        PGDB.__init__(self)
        self.session = requests.Session()
        self.home_url = self.home_url.format(donglehost)
        self.ussd_token = None
        self.sms_token = None
        self.headers = None
        self.connected = False

    @staticmethod
    def check_dongle(func):
        """
        Check if usb dongle is inserted
        :param func:
        :return: Boolean
        """
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            try:
                if self.session.get(self.home_url,
                                    timeout=3).status_code == 200:
                    self.connected = True
                    return func(self, *args, **kwargs)
            except requests.exceptions.Timeout:
                LOG.debug('Connection timed out')
                sql = 'insert into errors (time, error) values (\'{}\', ' \
                      '\'{}\')'.format(datetime.datetime.now(),
                                       'Нет связи с модемом. Таймаут.')
                self.execute_sql(sql)
                self.connected = False
                raise requests.exceptions.Timeout
            except requests.exceptions.ConnectionError:
                LOG.debug('Connection error')
                sql = 'insert into errors (time, error) values (\'{}\', ' \
                      '\'{}\')'.format(datetime.datetime.now(),
                                       'Нет связи с модемом. '
                                       'Ошибка соединения.')
                self.execute_sql(sql)
                self.connected = False
                raise requests.exceptions.Timeout
        return wrapper

    def execute_sql(self, sql):
        """
        Run sql
        :param sql: string with sql
        :return: None
        """
        self.db_connection = psycopg2.connect(**self.dbsetup)
        self.cursor = self.db_connection.cursor()
        self.cursor.execute(sql)
        self.db_connection.commit()
        self.db_connection.close()

    @staticmethod
    def check_network(func):
        """
        Check if usb dongle is connected to cellular network
        :param func:
        :return:
        """
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            resp = xmltodict.parse(self.session.get(
                self.home_url + self.monitoring_status).content)
            if int(resp['response']['ConnectionStatus']) == 901:
                LOG.info('network online')
                return func(self, *args, **kwargs)
            LOG.debug('No signal.')
            sql = 'insert into errors (time, error) values (\'{}\',' \
                  ' \'{}\')'.format(datetime.datetime.now(),
                                    'Нет связи с сим картой.')
            self.execute_sql(sql)
            return None

        return wrapper

    @staticmethod
    def refresh_sms_token(func):
        """
        Refresh sms token
        :param func:
        :return:
        """
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            self.get_sms_token()
            time.sleep(0.5)
            return func(self, *args, **kwargs)
        return wrapper

    @check_dongle
    def get_sms_token(self):
        """
        Get current sms token
        :return:
        """
        try:
            html = BeautifulSoup(self.session.get(
                self.home_url + self.sms_url).content, 'html.parser')
            self.sms_token = html.find('meta').get('content')
            self.headers = {'__RequestVerificationToken': self.sms_token,
                            "X-Requested-With": "XMLHttpRequest",
                            "Content-Type": 'text/xml'}
        except Exception:
            LOG.debug('Failed to update token.')
            sql = 'insert into errors (time, error) values (\'{}\',' \
                  ' \'{}\')'.format(datetime.datetime.now(),
                                    'Не удалось обновить токен.')
            self.execute_sql(sql)

    @staticmethod
    def refresh_ussd_token(func):
        """
        Refresh current ussd token
        :param func:
        :return:
        """
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            self.get_ussd_token()
            time.sleep(0.5)
            return func(self, *args, **kwargs)
        return wrapper

    @check_dongle
    def get_ussd_token(self):
        """
        Get current ussd token and set as server property
        :return: None
        """
        try:
            html = BeautifulSoup(self.session.get(
                self.home_url + self.ussd_url).content, 'html.parser')
            self.ussd_token = html.find('meta').get('content')
            self.headers = {'__RequestVerificationToken': self.ussd_token,
                            "X-Requested-With": "XMLHttpRequest",
                            "Content-Type": 'text/xml'}
        except Exception:
            LOG.debug('Failed to update token.')
            sql = 'insert into errors (time, error) values (\'{}\',' \
                  ' \'{}\')'.format(datetime.datetime.now(),
                                    'Не удалось обновить ussd токен.')
            self.execute_sql(sql)

    def check_new_sms(self):
        """
        Check for new sms on usb dongle
        :return: list of new sms
        """
        response = self.make_api_get_request(
            self.home_url + self.sms_count_url)
        if response is not None:
            new_sms_count = int(response['LocalUnread'])
            total_sms = int(response['LocalInbox'])
            if new_sms_count > 0:
                LOG.info('Received %s new sms', str(new_sms_count))
                data = self.get_sms_xml.format(str(total_sms))
                messages = self.make_api_post_request(
                    url=self.home_url + self.sms_receive_url,
                    data=data)['Messages']['Message']
                if isinstance(messages, OrderedDict):
                    messages = [messages]
                sms_list = [SMS(**message) for message in messages]
                _ = [LOG.info('%s - %s - %s', sms.index,
                              sms.phone,
                              sms.content) for sms in
                     sms_list]
                return sms_list
        return []

    def save_new_sms_in_db(self, messages):
        """
        Save incoming sms to database
        :param messages: list of sms
        :return: list of sms
        """
        for sms in messages:
            if sms.smstat == '0':
                LOG.info('saving to db %s', sms.index)
                self.db_connection = psycopg2.connect(**self.dbsetup)
                self.cursor = self.db_connection.cursor()
                self.cursor2 = self.db_connection.cursor(
                    cursor_factory=extras.DictCursor)
                processed = sms.smstype == 2
                self.cursor.execute(sms.save_sql(),
                                    (sms.index,
                                     sms.phone,
                                     sms.content,
                                     sms.date,
                                     sms.sca,
                                     sms.savetype,
                                     sms.priority,
                                     sms.smstype,
                                     processed))
                self.db_connection.commit()
                self.db_connection.close()
                sms.smstat = '1'
        return messages

    @refresh_sms_token
    def make_api_get_request(self, url):
        """
        Send post request
        :param url: api address
        :return: response
        """
        if self.connected:
            return xmltodict.parse(self.session.get(url=url,
                                                    headers=self.headers).text,
                                   encoding='utf-8')['response']
        return None

    @refresh_sms_token
    def make_api_post_request(self, url, data):
        """
        Send post request and log response
        :param url: api address
        :param data: string with sms
        :return: response
        """
        response = xmltodict.parse(
            self.session.post(url=url,
                              data=data.encode('utf-8'),
                              headers=self.headers).content.decode('utf-8'),
            encoding='utf-8')
        LOG.info(response)
        if 'response' in response.keys():
            return response['response']
        return response

    @refresh_ussd_token
    def make_api_ussd_get_request(self, url):
        """
        Send get request with ussd command
        :param url: api address string
        :return:
        """
        if self.connected:
            return xmltodict.parse(
                self.session.get(url=url,
                                 headers=self.headers).text,
                encoding='utf-8')['response']
        return None

    @refresh_ussd_token
    def make_api_ussd_post_request(self, url, data):
        """
        Send post request with ussd command
        :param url: api address string
        :param data: string
        :return: response object
        """
        response = xmltodict.parse(
            self.session.post(url=url,
                              data=data.encode('utf-8'),
                              headers=self.headers).content.decode('utf-8'),
            encoding='utf-8')
        return response['response']

    def set_read_on_dongle(self, sms_list):
        """
        Set sms status to 'read' on usb dongle
        :param sms_list:
        :return:
        """
        result = {}
        for sms in sms_list:
            if sms.smstat == '0':
                data = self.sms_id_xml.format(sms.index)
                result[sms.index] = self.make_api_post_request(
                    url=(self.home_url + self.sms_set_read_url),
                    data=data)
        if result:
            LOG.info('marked read on dongle : %s', str(result))

    def delete_from_dongle_incoming(self, sms_list):
        """
        Delete incoming sms from usb dongle
        :param sms_list:
        :return:
        """
        result = {}
        for sms in sms_list:
            data = self.sms_id_xml.format(sms.index)
            result[sms.index] = self.make_api_post_request(
                url=(self.home_url + self.sms_delete_url),
                data=data)
        return result

    def check_sms_to_send(self):
        """
        Check for incoming sms on usb dongle
        :return:
        """
        if self.db_connected:
            self.db_connection = psycopg2.connect(**self.dbsetup)
            self.cursor = self.db_connection.cursor()
            # self.cursor2 = self.db_connection.cursor(cursor_factory=extras.DictCursor)
            sql = 'SELECT index, content, (select phone from contacts ' \
                  'where id = sms_to_send.contact) as phone ' \
                  'from sms_to_send;'
            self.cursor.execute(sql)
            smstosend = self.cursor.fetchall()
            messages = [dict(zip(('index', 'content', 'phone'), row))
                        for row in smstosend]
            sms_to_send = [SMS(**message) for message in messages]
            self.cursor.execute(sql)
            self.db_connection.close()
            return sms_to_send
        return []

    def send_sms_list(self, sms_list):
        """
        Send sms via usb dongle
        :param sms_list:
        :return:
        """
        result = []
        for sms in sms_list:
            data = self.sms_send_xml.format(sms.phone,
                                            sms.content,
                                            sms.len,
                                            sms.date)
            resp = self.make_api_post_request(
                url=self.home_url + self.sms_send_url,
                data=data)
            LOG.info(resp)
            if resp == 'OK':
                result.append(sms)
                LOG.info('Sent sms %s - %s', str(sms.index),
                         sms.content)
                time.sleep(3)
        return result

    def record_sent_sms(self, sent_sms):
        """
        Save sms sent via usb dongle to database
        :param sent_sms:
        :return:
        """
        for sms in sent_sms:
            sql = 'SELECT * from sms_to_send where index= {}'.format(sms.index)
            self.db_connection = psycopg2.connect(**self.dbsetup)
            self.cursor = self.db_connection.cursor()
            self.cursor.execute(sql)
            if self.cursor.fetchall():
                sql = 'INSERT INTO sentmessages (index, phone, content, date)'\
                      ' values (%s, %s, %s,%s);'
                self.cursor.execute(sql, (sms.index,
                                          sms.phone,
                                          sms.content,
                                          sms.date))
                sql = 'DELETE FROM sms_to_send where ' \
                      'index = {}'.format(sms.index)
                self.cursor.execute(sql)
                self.db_connection.commit()
            self.db_connection.close()

    def report_last_access(self):
        """

        :return:
        """
        self.db_connection = psycopg2.connect(**self.dbsetup)
        self.cursor = self.db_connection.cursor()
        sql = 'select lastaccess from status'
        self.cursor.execute(sql)
        last_access = self.cursor.fetchall()
        if not last_access:
            sql = "insert into status values(\'{}\')"
        else:
            sql = "UPDATE status set lastaccess = \'{}\'"
        self.cursor.execute(sql.format(datetime.datetime.now()))
        self.db_connection.commit()
        self.db_connection.close()

    def check_ussd_to_send(self):
        """
        Check for ussd request responses on dongle
        :return:
        """
        sql = 'select id, request from ussdrequest'
        self.db_connection = psycopg2.connect(**self.dbsetup)
        self.cursor = self.db_connection.cursor()
        self.cursor.execute(sql)
        ussd_requests = self.cursor.fetchall()
        self.db_connection.close()
        return ussd_requests

    def send_ussd_requests(self, ussd_requests):
        """
        Send ussd requests via dongle
        :param ussd_requests:
        :return:
        """
        result = []
        for request in ussd_requests:
            data = self.ussd_send_xml.format(request['request'])
            if self.make_api_ussd_post_request(
                    url=self.home_url + self.ussd_send_api,
                    data=data) == 'OK':
                result.append(request)
                LOG.info('Sent sms %s', request['request'])
        return result

    @staticmethod
    def run_commands(commands):
        """
        Run shell commands
        :param commands:
        :return:
        """
        LOG.info('Got commands %s', commands)
        for command in commands:
            if command[1] == 'restart.sh':
                subprocess.run([command[1], psutil.Process().pid])
            else:
                subprocess.run(command[1])

    def check_commands(self):
        """
        Check for shell command in database
        :return:
        """
        self.db_connection = psycopg2.connect(**self.dbsetup)
        self.cursor = self.db_connection.cursor()
        sql = 'select id, command from commands'
        self.cursor.execute(sql)
        commands = self.cursor.fetchall()
        if commands:
            LOG.info('deleting')
            for command in commands:
                sql = 'delete from commands where id=%s;'
                self.cursor.execute(sql, command[0])
                self.db_connection.commit()
        return commands

    def delete_from_dongle_sent_sms(self):
        """
        Delete sms records on usb dongle
        :return:
        """
        response = self.make_api_get_request(
            self.home_url + self.sms_count_url)
        if response is not None:
            sent_sms_count = int(response['LocalOutbox'])
            if sent_sms_count:
                LOG.info('%s sent sms found on dongle', sent_sms_count)
                while sent_sms_count > 0:
                    data = self.get_sent_sms_xml.format(str(1))
                    messages = self.make_api_post_request(
                        url=self.home_url + self.sms_receive_url,
                        data=data)['Messages']['Message']
                    if isinstance(messages, OrderedDict):
                        messages = [messages]
                    sms_list = [SMS(**message) for message in messages]
                    result = {}
                    for sms in sms_list:
                        data = self.sms_id_xml.format(sms.index)
                        LOG.info('deleting  sms %s %s', str(sms.index),
                                 sms.content)
                        result[sms.index] = self.make_api_post_request(
                            url=(self.home_url + self.sms_delete_url),
                            data=data)
                    sent_sms_count -= 1
        return []

    def run(self):
        """
        Run server loop
        :return:
        """
        while True:
            self.report_last_access()
            sms_list = self.check_new_sms()
            self.set_read_on_dongle(sms_list)
            if self.db_connected and sms_list:
                sms_list = self.save_new_sms_in_db(sms_list)
                self.delete_from_dongle_incoming(sms_list)
            if self.db_connected:
                sms_to_send = self.check_sms_to_send()
                sent_sms = self.send_sms_list(sms_to_send)
                self.record_sent_sms(sent_sms)
                self.delete_from_dongle_sent_sms()
            ussd_requests_to_send = self.check_ussd_to_send()
            if ussd_requests_to_send:
                self.send_ussd_requests(ussd_requests_to_send)
            commands = self.check_commands()
            if commands:
                self.run_commands(commands)
            time.sleep(10)


if __name__ == '__main__':
    try:
        server = Server(donglehost='192.168.8.1')
        server.run()
    except KeyboardInterrupt:
        server.db_connection.close()
