from authapp.forms import LesorubUserLoginForm
from django.shortcuts import render
from django.http import HttpResponse
from django.core.mail import send_mail

from .models import NewsArticle, InstagramPost


def index(request):
    login_form = LesorubUserLoginForm(data=request.POST or None)
    lastnewslist = NewsArticle.objects.all().order_by('-postdate')[:3]
    posts = InstagramPost.objects.all().order_by('-created')[:4]
    description = "LONG MULTILINE TEXT"
    context = {'title': 'Лесоруб ',
               'login_form': login_form,
               'description': description,
               'newslist': lastnewslist,
               'posts': posts,
               }
    return render(request, 'mainapp/xxx.html', context)


def rules(request):
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'Лесоруб',
               'description': description,
               }
    return render(request, 'mainapp/xxx.html', context)


def contact(request):
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'Лесоруб',
               'description': description,
               }
    return render(request, 'mainapp/xxx.html', context)


def privacy(request):
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'Лесоруб',
               'description': description,
               }
    return render(request, 'mainapp/xxx.html', context)


def newslist(request):
    articles = NewsArticle.objects.all().order_by('-postdate')
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'Лесоруб',
               'description': description,
               'articles': articles,
               }
    return render(request, 'mainapp/xxx.html', context)


def news(request, pk):
    article = NewsArticle.objects.get(pk=pk)
    latestnews = NewsArticle.objects.all().order_by('-postdate')[:3]
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'Лесоруб',
               'latestnews': latestnews,
               'description': description,
               'article': article,
               'text': article.text.split('\n'),
               }
    return render(request, 'mainapp/news.html', context)


def cookies(request):
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'Лесоруб',
               'description': description,
               }
    return render(request, 'mainapp/xxx.html', context)


def callme(request):
    try:
        phone = request.POST['myphone']
        path = request.POST['path']
        send_mail('Запрос на звонок', str(phone) + '\n' + str(path),
                  'mail@mail.com', ['mail@mail.com'])
        response = HttpResponse(status=200, content='success')
    except Exception:
        response = HttpResponse(status=400, content='failed')
    return response


def book(request):
    try:
        phone = request.POST['phone']
        path = request.POST['path']
        date = request.POST['date']
        time = request.POST['time']
        comments = request.POST['comments']
        message = ' '.join(('Запрос брони\n',
                            str(phone),
                            '\n',
                            str(time),
                            str(date),
                            '\n',
                            str(comments),
                            '\n\n', str(path)))
        send_mail('Запрос на звонок',
                  message,
                  'lesorubmoscow@gmail.com',
                  ['lesorubmoscow@gmail.com'])
        response = HttpResponse(status=200, content='success')
    except Exception:
        response = HttpResponse(status=400, content='failed')
    return response


def birthday(request):
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'LONG MULTILINE TEXT',
               'description': description}
    return render(request, 'mainapp/xxx.html', context)


def birthday2(request):
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'LONG MULTILINE TEXT',
               'description': description}
    return render(request, 'mainapp/xxx.html', context)


def games(request):
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'Лесоруб',
               'description': description,
               }
    return render(request, 'mainapp/xxx.html', context)


def giftcards(request):
    description = 'LONG MULTILINE TEXT'
    context = {'title': 'Лесоруб',
               'description': description,
               }
    return render(request, 'mainapp/xxx.html', context)


def sitemap(request):
    context = {}
    return render(request, 'mainapp/sitemap.xml', context)
