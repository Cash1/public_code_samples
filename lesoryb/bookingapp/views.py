import datetime
import time
import logging
from pandas import date_range

from django.shortcuts import render
from django.db import connection
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.db import transaction
from django.core.mail import send_mail

from bookingapp.models import Booking, Order
from mainapp.models import Arena

LOGGER = logging.getLogger('mainlogger')


def schedule(request):

    """
    Сырой SQL, т.к. возникли сложности в передаче параметров в generate_series.
    """

    if request.method == 'GET':
        sql = 'select booking.arenaname_id as arena, b.duration, ' \
              'case when sum(booking.qtyofguests) is null then 0 else sum(booking.qtyofguests) end as qty ' \
              'from (' \
              'select qtyofguests, tsrange(closed, lead(closed, 1) ' \
              'over (partition by qtyofguests order by closed)) as duration ' \
              'from generate_series(%s::timestamp, %s::timestamp, \'1 hours\') as closed, (values (0::int)) ' \
              'as qtyofguests) as b ' \
              'left join ' \
              'booking on b.duration = booking.duration ' \
              'where (extract(hour from lower(b.duration))>= 11) and (extract(hour from lower(b.duration)) < 22) ' \
              'group by b.duration, booking.arenaname_id order by lower(b.duration) ' \
              'AT TIME ZONE \'Europe/Moscow\';'
        with connection.cursor() as cursor:
            if request.GET.__contains__('from') and \
                    request.GET.__contains__('to'):
                cursor.execute(sql,
                               (request.GET['from'], request.GET['to']))
                drange = date_range(start=datetime.datetime.strptime(
                    request.GET['from'], '%Y-%m-%d'), periods=7, freq='D')
            else:
                cursor.execute(sql,
                               (datetime.date.today().strftime('%Y-%m-%d'),
                                (datetime.datetime.today() + datetime.timedelta(days=7)).strftime('%Y-%m-%d')))
                drange = date_range(start=datetime.date.today(),
                                    periods=7,
                                    freq='D')
            sched = dictfetchall(cursor)
    arenas = Arena.objects.all()

    days = []
    for day in drange:
        slots = []
        dayslots = {}
        dayslots['date'] = day.date().strftime('%d-%m-%Y')
        dayslots['weekday'] = day.weekday()
        for booking in sched:
            if booking['duration'].lower.date() == day.date():
                for arena in arenas:
                    slot = Slot(arena, booking)
                    slots.append(slot.__dict__)
        dayslots['slots'] = slots
        days.append(dayslots)
    context = {'days': days,
               'rooms': arenas,
               'title': 'Лесоруб',
               }
    return render(request, 'mainapp/xxx.html', context)


class Slot:
    def __init__(self, arena, booking):
        self.time = booking['duration'].lower.strftime('%H:%M')
        self.arena = arena.name
        self.arena_rusname = arena.rusname
        self.vacant = 8
        if booking['arena'] is not None:
            self.vacant -= booking['qty']

    def __dict__(self):
        return {'time': self.time,
                'vacant': self.vacant,
                'arena': self.arena,
                'arena_rusname': self.arena_rusname}


def make_booking(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            order = Order(booker=request.user,)
            order.save()
            try:
                with transaction.atomic():
                    msg = []
                    for slot in range(0, int(request.POST['slotqty'])):
                        bookingdatetime = datetime.datetime.combine(
                            datetime.datetime.strptime(
                                request.POST['bookingdate' + str(slot)],
                                '%d-%m-%Y').date(),
                            datetime.datetime.strptime(
                                request.POST['bookingtime' + str(slot)],
                                '%H:%M').time())
                        booking = Booking(
                            arenaname=Arena.objects.get(
                                rusname=request.POST['arenaname' + str(slot)]),
                            bookdatetime=bookingdatetime,
                            duration=[bookingdatetime,
                                      bookingdatetime + datetime.timedelta(minutes=60)],
                            qtyofguests=request.POST['qtyofguests' + str(slot)],
                            order_id=order, )
                        booking.save()
                        msg.append([booking.bookdatetime.strftime('%d-%m-%Y %H:%M'),
                                    booking.qtyofguests])
                response = HttpResponse(status=200, content='success')
                mailtext = 'Ваша бронь на '
                if len(msg) > 1:
                    for booking in msg:
                        mailtext += ''.join([booking[0],
                                             ' на ',
                                             booking[1],
                                             ' человек, на '])
                    mailtext = mailtext[:-5]
                else:
                    mailtext += ''.join([msg[0][0],
                                         ' на ',
                                         msg[0][1],
                                         ' человек'])
                mailtext += '.\nЖдем Вас!'
                send_mail('Бронирование топор-бар "Лесоруб"',
                          mailtext,
                          'mail@mail.com',
                          [request.user.email])
            except Exception as err:
                LOGGER.error(err)
                response = HttpResponse(status=500, content='Database error')
    else:
        response = HttpResponse(status=401, content='User not authenticated')
    time.sleep(3)

    return response


@login_required
def checkuser(request):
    if request.user.is_authenticated:
        response = HttpResponse(True, status=200)
    else:
        response = HttpResponse(False, status=403)
    return response


@login_required
def managebookings(request):
    context = {'title': 'Управление бронированием'}
    return render(request, 'mainapp/managebookings.html', context)


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
