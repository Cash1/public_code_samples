from django.contrib import admin
from mainapp.models import Arena, NewsArticle, NewsTag, InstagramPost

# Register your models here.


@admin.register(Arena)
class ArenaAdmin(admin.ModelAdmin):
    list_display = ('name',)
    pass


@admin.register(NewsArticle)
class NewsArticleAdmin(admin.ModelAdmin):
    list_display = ('title',)
    pass


@admin.register(NewsTag)
class NewsTagAdmin(admin.ModelAdmin):
    list_display = ('tag',)
    pass


@admin.register(InstagramPost)
class InstagramPostAdmin(admin.ModelAdmin):
    list_display = ('code',)
    pass
