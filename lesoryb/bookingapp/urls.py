from django.urls import re_path

import bookingapp.views as views

app_name = 'bookingapp'

urlpatterns = [
    re_path('^new$', views.make_booking, name='new'),
    re_path('^schedule/$', views.schedule, name='schedule'),
    re_path('^checkuser/$', views.checkuser, name='checkuser'),
    re_path('^managebookings/$', views.managebookings, name='managebookings'),
    ]
